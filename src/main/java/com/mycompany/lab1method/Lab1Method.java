/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1method;
import java.util.Scanner;
/**
 *
 * @author informatics
 */
public class Lab1Method {

    public static void main(String[] args) {
        for(turn = 1; turn < 10; turn++) {
            printTable();
            printTurn();
            inputRowCol();
            if(checkWin()){
                printWin();
                break;
            }
            if (turn == 9) {
                printDraw();
                break;
            }
            switchPlayer();
        }
    }
    
    static char[][] tic = {{'-', '-', '-'},{'-', '-', '-'},{'-', '-', '-'}};
    static char player = 'X';
    static int row,col;
    static int turn;
    
    public static void printWelcome() {
        System.out.println("Welcome to XO RowCol Games!");
    }
    
    public static void printTable() {
        for (int i = 0; i < 3; i++ ){
            for (int j = 0; j < 3; j++){
                System.out.print(tic[i][j] + " ");
            }
            System.out.println(" ");
        }
    }
    
    public static void printDraw() {
        System.out.println("Plaer Draw!");
    }
    
    public static void printTurn() {
        System.out.println("Turn " + player);
    }
    
    public static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input row col:");
        row = sc.nextInt();
        col = sc.nextInt();
        if (checkAlready(row, col)) {
            System.out.println("This spot alreay have tic");
            printTable();
            System.out.println("Please input RowCol again");
            inputRowCol();
        }
        tic[row][col] = player;
    }
    
    public static void switchPlayer() {
        if (player == 'X') player = 'O';
        else player = 'X';
    }
    
    public static boolean checkAlready(int r, int c) {
        return tic[r][c] == 'X' || tic[r][c] == 'O';
    }
    
    static boolean checkWinRow() {
        for (int c = 0; c < 3; c++) {
            if (tic[row][c] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkWinCol() {
        for (int r = 0; r < 3; r++) {
            if (tic[r][col] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCross1() {
        return tic[0][0] == player && tic[1][1] == player && tic[2][2] == player;
    }

    static boolean checkCross2() {
        return tic[0][2] == player && tic[1][1] == player && tic[2][0] == player;
    }
    
    static boolean checkWin() {
        return checkWinCol() || checkWinRow() || checkCross1() || checkCross2();
    }
    
    public static void printWin() {
        System.out.println("Player: " + player + " Win!!!");
    }
}
